(defproject rasta "0.1.0-SNAPSHOT"
  :description "La rasta de Andres Gu"
  :url "https://gitlab.com/nez/rasta"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [clj-time "0.15.1"]
                 [comun "0.1.0-SNAPSHOT"]
                 [digest "1.4.9"]
                 [digitalize "0.1.0-SNAPSHOT"]
                 [formaterr "1.0.0-SNAPSHOT"]
                 [mongerr "1.0.0-SNAPSHOT"]
                 [me.raynes/conch "0.8.0"]
                 [environ "1.1.0"]
                 [me.raynes/fs "1.4.6"]
                 [http-kit "2.3.0"]
                 [com.cemerick/url "0.1.2-SNAPSHOT"]]
  :repl-options {:init-ns rasta.core})
